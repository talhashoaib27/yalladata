require 'mechanize'
require 'aws-sdk'
require 'csv'
class PriceExtractor < Struct.new(:url, :file_name, :file_key)

    
  def perform
    price_file = Price.find_by_file_key(file_key)
    if price_file.present?
      models_with_prices = PriceExtractor.fetch_pricing(url)
      csv = PriceExtractor.make_csv(models_with_prices)
      PriceExtractor.upload_to_s3(csv, file_name)
      price_file.file_path = "https://s3-us-west-2.amazonaws.com/yalladata/uploads/#{file_name}.csv"
      price_file.save!
    end
  end


	def self.fetch_pricing(url)
    mechanize = Mechanize.new
    visited = []
    models_with_prices = []
    car_makers_url = url
    car_maker_name = url.split('/').last

    car_makers_page = mechanize.get(car_makers_url)

    car_makers_page.links_with(href: /\/new-cars\//).each do |link|
      unless ['2017', '2016', '2015', '2014', '2013', '2012'].include? link.href.split('/').last 
        if link.href.include?("/new-cars/#{car_maker_name}/") && !visited.include?(link.href)
          model_page = mechanize.click(link)
          models_with_prices << { type: "Car Model",
                                  title: "#{car_maker_name} #{link.href.split('/').last}".titleize
                                 }            
          pricing = model_page.search("div.prices-and-specs")
          pricing.search("div.new-cars-results-text").each do |versions|
            models_with_prices << { type: "Car Version",
                                    title: versions.search('h4').first.search('a').text.strip, 
                                    price: versions.search('h4').first.search('span').text.strip }
          end
          visited << link.href
        end
      end
    end
    models_with_prices
	end

  def self.make_csv(data)
    column_names = ['TYPE', 'TITLE', 'PRICE']
      prices_csv = CSV.generate do |csv|
        csv << column_names
        data.each do |item|
          csv << [item[:type], item[:title], item[:price]]
        end
      end
    prices_csv
  end

  def self.upload_to_s3(data, file_name)
    Aws.config[:credentials] = Aws::Credentials.new(ENV['S3_KEY'], ENV['S3_SECRET'])
    s3 = Aws::S3::Resource.new(region:'us-west-2')
    obj = s3.bucket('yalladata').object("uploads/#{file_name}.csv")
    obj.put body: data
  end
end