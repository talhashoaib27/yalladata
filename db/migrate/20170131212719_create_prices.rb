class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.string :file_key
      t.string :file_path

      t.timestamps null: false
    end
    add_index :prices, :file_key, unique: true
  end
end
