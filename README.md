# Yalladata #

A simple program to fetch latest car prices of a car manufacturer given at 
e.g. uae.yallamotor.com/new-cars. 

### Assumptions ###

* The HTML DOM structure will be same.
* The URL structure of yallamotor.com will be same. i.e. yallamotor.com/new-cars/<car_manufacturer>/<car_model>
* It's only to fetch latest(2017) prices.


### Heroku App ###

* yalladata url: `https://yalladata.herokuapp.com`

### Local Setup ###

* 
* clone repository
* replace database.yml.example with database.yml and provide postgresql credentials in it.
* create database `rake db:create`
* migrate database `rake db:migrate`
* open background jobs `rake jobs:work`
* Provide S3 credentials in `lib/price_extractor.rb`
* Start Server `rails s`
* Start fetching new prices of cars :)
* example url: `http://uae.yallamotor.com/new-cars/bmw`