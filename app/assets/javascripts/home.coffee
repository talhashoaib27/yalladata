# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).ready ->
  $('form').submit ->
    $('#page-loader').removeClass 'display-none'
    $('.form-submit').attr('disabled', 'disabled')
    return
  $('.page-reload').click ->
	  location.reload()
	  return
  return