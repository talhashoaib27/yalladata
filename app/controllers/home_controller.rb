require 'open-uri'
class HomeController < ApplicationController
	# include PriceExtractor

	before_action :validate_url, only: [:csv]
	
  def index
  end

  def csv
  	begin
  	  random_str = get_random_hex
  		filename = params[:url].split('/').last+"_"+random_str 
	  	Delayed::Job.enqueue(PriceExtractor.new(params[:url], filename, random_str))
      price_file = Price.new(file_key: random_str)
      if price_file.save
	      flash[:success] = "Your file is being prepared! Relax."
				redirect_to download_path(random_str)
			else
				flash[:alert] = "Try again in few moments."
				redirect_to :back
			end
		rescue
			flash[:alert] = "Error in fetching results."
			redirect_to :back
		end
  end

  def download
  	price_file = Price.find_by_file_key(params[:id])
  	if price_file.present? && price_file.file_path.present?
	  	filename = price_file.file_path[53..price_file.file_path.length]
	  	data = open(price_file.file_path)
		  send_data data.read, :type => "text/plain", 
			                      :filename=>"#{filename}",
			                      :disposition => 'attachment'
    elsif price_file.present?
    	# flash['info'] = "It is being uploaded. Try in few minutes: https://yalladata.herokuapp.com/download/#{params[:id]}"
    	# redirect_to root_path
    else
    	flash[:alert] = "Invalid File Path."
    	redirect_to root_path
    end
end 

  private
  def get_random_hex
  	SecureRandom.hex(10).to_s
  end

  def validate_url
  	unless params[:url].present? && params[:url].include?('yallamotor.com') &&
  		     (params[:url].split(':')[0] == 'http' || params[:url].split(':')[0] == 'https')
  		flash[:alert] = "Invalid URL."
			redirect_to :back
		end
  end
end
		   